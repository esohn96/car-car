import * as React from "react";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import MenuIcon from "@mui/icons-material/Menu";
// import FeaturedPlaylistIcon from "@mui/icons-material/FeaturedPlaylist";
import MiscellaneousServicesIcon from "@mui/icons-material/MiscellaneousServices";
import ListAltIcon from "@mui/icons-material/ListAlt";
import HomeIcon from "@mui/icons-material/Home";
import ContactsIcon from "@mui/icons-material/Contacts";
import logoImg from "../media/logo.png";
import { Container } from "@mui/system";
import Menu from "@mui/material/Menu";
import MenuItem from "@mui/material/MenuItem";
import CustomButton from "./CustomButton";
import {
  Drawer,
  List,
  ListItem,
  ListItemButton,
  ListItemIcon,
  ListItemText,
  styled,
} from "@mui/material";
import { useState } from "react";
import { NavLink } from "react-router-dom";

export const Navbar = () => {
  const [anchorEl, setAnchorEl] = useState(null);
  const [openMenu, setOpenMenu] = useState(null);

  const handleClick = (menuName) => (event) => {
    setAnchorEl(event.currentTarget);
    setOpenMenu(menuName);
  };

  const handleClose = () => {
    setAnchorEl(null);
    setOpenMenu(null);
  };

  const Navbar = styled(Typography)(({ theme }) => ({
    fontSize: "14px",
    color: "#4F3561",
    fontWeight: "bold",
    cursor: "pointer",
    "&:hover": {
      color: "#fff",
    },
  }));

  const NavbarLinksBox = styled(Box)(({ theme }) => ({
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    gap: theme.spacing(3),
    [theme.breakpoints.down("md")]: {
      display: "none",
    },
  }));

  const CustomMenuIcon = styled(MenuIcon)(({ theme }) => ({
    cursor: "pointer",
    display: "none",
    marginRight: theme.spacing(2),
    [theme.breakpoints.down("md")]: {
      display: "block",
    },
  }));

  const NavbarContainer = styled(Container)(({ theme }) => ({
    display: "flex",
    alignitems: "center",
    justifyContent: "space-between",
    padding: theme.spacing(5),
    [theme.breakpoints.down("md")]: {
      padding: theme.spacing(2),
    },
  }));

  const NavbarLogo = styled("img")(({ theme }) => ({
    cursor: "pointer",
    [theme.breakpoints.down("md")]: {
      display: "none",
    },
  }));

  return (
    <NavbarContainer>
      <Box
        sx={{
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
          gap: "2.5rem",
        }}
      >
        <Box sx={{ display: "flex", alignItems: "center" }}>
          <CustomMenuIcon />
          <NavbarLogo src={logoImg} height="100px" width="100px" alt="logo" />
        </Box>
        <NavbarLinksBox>
          <NavLink to="/">Home</NavLink>
          <Typography
            variant="body2"
            onClick={handleClick("inventory")}
            style={{ cursor: "pointer" }}
          >
            Inventory
          </Typography>
          <Menu
            id="inventory-menu"
            anchorEl={anchorEl}
            open={openMenu === "inventory"}
            onClose={handleClose}
          >
            <NavLink to="/manufacturers/">
              <MenuItem>Manufacturer List</MenuItem>
            </NavLink>
            <NavLink to="/manufacturers/new/">
              <MenuItem>Create manufacturer</MenuItem>
            </NavLink>
            <NavLink to="/models/">
              <MenuItem>Vehicle Model List</MenuItem>
            </NavLink>
            <NavLink to="/models/new/">
              <MenuItem>Create vehicle model</MenuItem>
            </NavLink>
            <NavLink to="/automobiles/">
              <MenuItem>Automobile List</MenuItem>
            </NavLink>
            <NavLink to="/automobiles/new/">
              <MenuItem>Create automobile</MenuItem>
            </NavLink>
          </Menu>
          <Typography
            variant="body2"
            onClick={handleClick("services")}
            style={{ cursor: "pointer" }}
          >
            Services
          </Typography>
          <Menu
            id="services-menu"
            anchorEl={anchorEl}
            open={openMenu === "services"}
            onClose={handleClose}
          >
            <NavLink to="/appointments/">
              <MenuItem>Appointment List</MenuItem>
            </NavLink>
            <NavLink to="/appointments/new/">
              <MenuItem>Create appointment</MenuItem>
            </NavLink>
            <NavLink to="/services/">
              <MenuItem>Service History</MenuItem>
            </NavLink>
          </Menu>
          <Typography
            variant="body2"
            onClick={handleClick("technicians")}
            style={{ cursor: "pointer" }}
          >
            Technicians
          </Typography>
          <Menu
            id="technician-menu"
            anchorEl={anchorEl}
            open={openMenu === "technicians"}
            onClose={handleClose}
          >
            <NavLink to="/technicians/">
              <MenuItem>Technician List</MenuItem>
            </NavLink>
            <NavLink to="/technicians/new/">
              <MenuItem>Create technician</MenuItem>
            </NavLink>
          </Menu>
          <Typography
            variant="body2"
            onClick={handleClick("sales")}
            style={{ cursor: "pointer" }}
          >
            Sales
          </Typography>
          <Menu
            id="sales-menu"
            anchorEl={anchorEl}
            open={openMenu === "sales"}
            onClose={handleClose}
          >
            <NavLink to="/sales/newsale">
              <MenuItem>New Sales Record</MenuItem>
            </NavLink>
            <NavLink to="/sales/saleshistory/">
              <MenuItem>View Sales History</MenuItem>
            </NavLink>
            <NavLink to="/sales/newcustomer/">
              <MenuItem>New Customer</MenuItem>
            </NavLink>
            <NavLink to="/sales/newemployee/">
              <MenuItem>New Sales Employee</MenuItem>
            </NavLink>
          </Menu>
        </NavbarLinksBox>
      </Box>
      <Box
        sx={{
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
          gap: "1rem",
        }}
      >
        <Typography variant="body2">Login</Typography>
        <CustomButton
          backgroundColor="#0F1B4C"
          color="#fff"
          buttonText="Register"
        ></CustomButton>
      </Box>
    </NavbarContainer>
  );
};

export default Navbar;
